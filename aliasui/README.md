# AliasUI

A web app to create and remove e-mail aliases managed by [docker-mailserver](https://docker-mailserver.github.io/docker-mailserver/latest/).

> I've made this small web app mainly to try [htmx](https://htmx.org/) with [FastAPI](https://fastapi.tiangolo.com/).
