import docker
import random
from collections import defaultdict

from fastapi import FastAPI, Request, Form
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles

container_name = "mailserver"


def container():
    client = docker.from_env()
    return client.containers.get(container_name)


def new_alias(recipient, alias):
    container().exec_run(f"setup alias add {alias} {recipient}")


def del_alias(recipient, alias):
    container().exec_run(f"setup alias del {alias} {recipient}")


def alias_recipient_seq():
    result = container().exec_run("setup alias list")
    alias_list = result.output.decode().strip().split("\n\n")
    for alias_line in alias_list:
        yield alias_line[2:].split()


def group_by_recipient(seq):
    d = defaultdict(set)
    for alias, recipient in seq:
        d[recipient].add(alias)
    return dict(d)


app = FastAPI()
templates = Jinja2Templates(directory="templates")

app.mount("/static", StaticFiles(directory="static"), name="static")


@app.get("/", response_class=HTMLResponse)
async def home(request: Request):
    return templates.TemplateResponse("home.html", {"request": request})


@app.get("/recipients", response_class=HTMLResponse)
def get_recipients(request: Request):
    return templates.TemplateResponse("recipients.html", {
        "request": request,
        "groups": group_by_recipient(alias_recipient_seq()).items(),
        "sorted": sorted,
    })


@app.post("/recipients/{recipient}", response_class=HTMLResponse)
def post_recipient(request: Request, recipient: str, prefix: str = Form(...)):
    alias = f"{username(prefix)}@{domain(recipient)}"
    new_alias(recipient, alias)
    return get_recipients(request)


def username(email):
    return email.split("@")[0].lower()


def domain(email):
    return email.split("@")[1].lower()


@app.delete("/recipients/{recipient}/{alias}", response_class=HTMLResponse)
def delete_alias(request: Request, recipient: str, alias: str):
    del_alias(recipient, alias)
    return get_recipients(request)
